﻿#region Disclaimer

// <copyright file="XmlSerializationAttributes.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

namespace RobinBird.Persistence.Xml.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Method)]
    public class XmlBeforeDeserializationAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class XmlAfterDeserializationAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class XmlAfterSerializationAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class XmlBeforeSerializationAttribute : Attribute
    {
    }
}