﻿#region Disclaimer
// <copyright file="XmlSerializationAttributeInfo.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion

namespace RobinBird.Persistence.Xml.Editor
{
    using System;
    using System.Xml.Serialization;
    using Persistence.Editor.Interfaces;

    /// <summary>
    /// Provides information about the Xml serializer.
    /// </summary>
    public class XmlSerializationAttributeInfo : ISerializationAttributeInfo
    {
        #region ISerializationAttributeInfo Implementation
        public Type GetIgnoreAttributeType()
        {
            return typeof (XmlIgnoreAttribute);
        }
        #endregion
    }
}