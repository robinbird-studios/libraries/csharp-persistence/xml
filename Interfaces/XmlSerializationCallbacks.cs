﻿#region Disclaimer

// <copyright file="XmlSerializationCallbacks.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

namespace RobinBird.Persistence.Xml.Interfaces
{
    public interface IXmlBeforeSerialization
    {
        void OnBeforeSerialize();
    }

    public interface IXmlAfterSerialization
    {
        void OnXmlAfterSerialize();
    }

    public interface IXmlBeforeDeserialization
    {
        void OnXmlBeforeDeserialize();
    }

    public interface IXmlAfterDeserialization
    {
        void OnAfterDeserialize();
    }
}