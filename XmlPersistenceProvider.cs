﻿#region Disclaimer
// <copyright file="XmlPersistenceProvider.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion

namespace RobinBird.Persistence.Xml
{
    using System;
    using System.IO;
    using RobinBird.Persistence.Runtime;

    public class XmlPersistenceProvider : IPersistenceProvider
    {
        private readonly XmlSerializer serializer;

        public XmlPersistenceProvider(XmlSerializer serializer)
        {
            this.serializer = serializer;
        }

        #region IPersistenceProvider Implementation
        public string Extension
        {
            get { return "xml"; }
        }

        public void Serialize(Stream s, object o, Type t)
        {
            serializer.Serialize(s, o);
        }

        public object Deserialize(Stream s, Type t)
        {
            return serializer.Deserialize(s);
        }

        public string GetPathWithExtension(string path)
        {
            return Path.ChangeExtension(path, Extension);
        }
        #endregion
    }
}