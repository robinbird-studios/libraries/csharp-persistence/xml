﻿#region Disclaimer

// <copyright file="TalosXmlSerializer.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

namespace RobinBird.Persistence.Xml
{
    using System;
    using System.Xml.Serialization;
    using Interfaces;

    public class XmlSerializer : System.Xml.Serialization.XmlSerializer
    {
        public XmlSerializer(Type type) : base(type)
        {
        }

        public XmlSerializer(Type type, Type[] extraTypes) :
            base(type, extraTypes)
        {
        }

        protected override void Serialize(object o, XmlSerializationWriter writer)
        {
            var beforeSerialize = o as IXmlBeforeSerialization;

            if (beforeSerialize != null)
            {
                beforeSerialize.OnBeforeSerialize();
            }

            base.Serialize(o, writer);

            var afterSerialize = o as IXmlAfterSerialization;

            if (afterSerialize != null)
            {
                afterSerialize.OnXmlAfterSerialize();
            }
        }

        protected override object Deserialize(XmlSerializationReader reader)
        {
            object o = base.Deserialize(reader);

            var afterDeserialize = o as IXmlAfterDeserialization;

            if (afterDeserialize != null)
            {
                afterDeserialize.OnAfterDeserialize();
            }

            return o;
        }
    }
}